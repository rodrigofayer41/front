import { createRouter, createWebHistory } from "vue-router";
import Login from "../components/Login.vue";
import MethodOne from "../components/MethodOne.vue";
import MethodTwo from "../components/MethodTwo.vue";
import MethodThree from "../components/MethodThree.vue";
import store from "../store";

const routes = [
    {
        path: "/login",
        name: "Login",
        component: Login,
    },
    {
        path: "/",
        name: "MethodOne",
        component: MethodOne,
        meta: {
            requiresAuth: true
        }
    },
    {
        path: "/methodTwo",
        name: "MethodTwo",
        component: MethodTwo,
        meta: {
            requiresAuth: true
        }
    },
    {
        path: "/methodThree",
        name: "MethodThree",
        component: MethodThree,
        meta: {
            requiresAuth: true
        }
    }
]

const router = createRouter({
    history: createWebHistory('/'),
    routes
})
router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (!store.getters.isLoggedIn) {
            next('/login')
        } else {
            next()
        }
    } else {
        next()
    }
})

export default router
