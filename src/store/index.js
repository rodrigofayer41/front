import {createStore} from "vuex";

const store = createStore({
    state: {
        user: null,
        token: null
    },
    mutations: {
        setUser(state, user) {
            state.user = user
        },
        setToken(state, token) {
            state.token = token
        }
    },
    getters: {
        isLoggedIn(state) {
            return !!state.token;
        },
        getToken(state) {
            return state.token
        }
    }
})

export default store
